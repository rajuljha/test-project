#include <iostream>
#include <stack>
#include <vector>

using namespace std;

// Function to perform depth-first traversal
void depthFirstTraversal(vector<vector<int>>& graph, int startNode) {
    int numNodes = graph.size();
    vector<bool> visited(numNodes, false);
    stack<int> nodeStack;

    // Push the start node onto the stack
    nodeStack.push(startNode);

    while (!nodeStack.empty()) {
        int currentNode = nodeStack.top();
        nodeStack.pop();

        // Process the current node if it hasn't been visited
        if (!visited[currentNode]) {
            cout << currentNode << " ";
            visited[currentNode] = true;

            // Push all unvisited neighbors of the current node onto the stack
            for (int neighbor : graph[currentNode]) {
                if (!visited[neighbor]) {
                    nodeStack.push(neighbor);
                }
            }
        }
    }
}

int main() {
    // Example graph represented as an adjacency list
    vector<vector<int> > graph = {
        {1, 2},     // Node 0 is connected to nodes 1 and 2
        {0, 3, 4},  // Node 1 is connected to nodes 0, 3, and 4
        {0, 5},     // Node 2 is connected to nodes 0 and 5
        {1},        // Node 3 is connected to node 1
        {1},        // Node 4 is connected to node 1
        {2}         // Node 5 is connected to node 2
    };

    int startNode = 0; // Starting node for depth-first traversal

    cout << "Depth-First Traversal: ";
    depthFirstTraversal(graph, startNode);

    return 0;
}
