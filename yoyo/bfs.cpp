#include <iostream>
#include <vector>
#include <queue>

using namespace std;

// Function to perform breadth-first traversal
void breadthFirstTraversal(vector<vector<int> >& graph, int startNode) {
    int numNodes = graph.size();
    vector<bool> visited(numNodes, false);
    queue<int> nodeQueue;

    // Enqueue the start node
    nodeQueue.push(startNode);

    while (!nodeQueue.empty()) {
        int currentNode = nodeQueue.front();
        nodeQueue.pop();

        // Process the current node if it hasn't been visited
        if (!visited[currentNode]) {
            cout << currentNode << " ";
            visited[currentNode] = true;

            // Enqueue all adjacent nodes of the current node
            for (int i = 0; i < graph[currentNode].size(); i++) {
                if (!visited[graph[currentNode][i]]) {
                    nodeQueue.push(graph[currentNode][i]);
                }
            }
        }
    }
}

int main() {
    // Create a graph
    vector<vector<int> > graph = {
        {1, 2},     // Node 0 is connected to nodes 1 and 2
        {0, 2, 3},  // Node 1 is connected to nodes 0, 2, and 3
        {0, 1, 4},  // Node 2 is connected to nodes 0, 1 , 4
        {1, 4},     // Node 3 is connected to node 1,4
        {2, 3, 5},  // Node 4 is connected to node 2,3,5
        {4, 6},     // Node 5 is connected to node 4, 6
        {5}         // Node 6 is connected to node 5
    };

    // Perform BFS
    breadthFirstTraversal(graph, 2);

    return 0;
}