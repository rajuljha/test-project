#include <iostream>
#include <vector>

class QuadraticProbingHashTable {
private:
    static const int TABLE_SIZE = 10;
    std::vector<int> table;

public:
    QuadraticProbingHashTable() {
        table.resize(TABLE_SIZE, -1); // Initialize all slots to -1 (indicating empty)
    }

    int hashFunction(int key, int attempt) {
        return (key + attempt * attempt) % TABLE_SIZE;
    }

    void insert(int key) {
        int attempt = 0;
        int index = hashFunction(key, attempt);

        while (table[index] != -1) {
            // Quadratic probing: move to the next slot using a quadratic function
            attempt++;
            index = hashFunction(key, attempt);
        }

        table[index] = key;
    }

    bool search(int key) {
        int attempt = 0;
        int index = hashFunction(key, attempt);

        while (table[index] != -1) {
            if (table[index] == key) {
                return true; // Key found
            }

            // Quadratic probing: move to the next slot using a quadratic function
            attempt++;
            index = hashFunction(key, attempt);
        }

        return false; // Key not found
    }

    void display() {
        std::cout << "Hash Table (Quadratic Probing): ";
        for (int i = 0; i < TABLE_SIZE; ++i) {
            if (table[i] != -1) {
                std::cout << table[i] << " ";
            } else {
                std::cout << "- ";
            }
        }
        std::cout << std::endl;
    }
};
// MIT license 
int main() {
    QuadraticProbingHashTable quadraticProbingTable;

    quadraticProbingTable.insert(5);
    quadraticProbingTable.insert(15);
    quadraticProbingTable.insert(25);
    quadraticProbingTable.insert(35);

    quadraticProbingTable.display();

    int keyToSearch = 15;
    if (quadraticProbingTable.search(keyToSearch)) {
        std::cout << keyToSearch << " found in the hash table." << std::endl;
    } else {
        std::cout << keyToSearch << " not found in the hash table." << std::endl;
    }

    return 0;
}
