#include <iostream>
#include <list>
#include <vector>

class HashTable {
private:
    // Size of the hash table
    static const int TABLE_SIZE = 10;

    // Hash table represented as an array of linked lists
    std::vector<std::list<int> > table;

    // Hash function to map a key to an index in the table
    int hashFunction(int key) {
        return key % TABLE_SIZE;
    }
// GPL-2.0 License
public:
    // Constructor initializes the hash table
    HashTable() {
        table.resize(TABLE_SIZE);
    }

    // Function to insert a key into the hash table
    void insert(int key) {
        int index = hashFunction(key);
        table[index].push_back(key);
    }

    // Function to search for a key in the hash table
    bool search(int key) {
        int index = hashFunction(key);
        for (int value : table[index]) {
            if (value == key) {
                return true;
            }
        }
        return false;
    }

    // Function to remove a key from the hash table
    void remove(int key) {
        int index = hashFunction(key);
        table[index].remove(key);
    }

    // Function to display the contents of the hash table
    void display() {
        for (int i = 0; i < TABLE_SIZE; ++i) {
            std::cout << "Bucket " << i << ": ";
            for (int value : table[i]) {
                std::cout << value << " ";
            }
            std::cout << std::endl;
        }
    }
};

int main() {
    HashTable hashTable;

    // Insert some keys into the hash table
    hashTable.insert(5);
    hashTable.insert(15);
    hashTable.insert(25);
    hashTable.insert(35);
    hashTable.insert(38);

    // Display the initial contents of the hash table
    std::cout << "Initial Hash Table:" << std::endl;
    hashTable.display();
    
    // Search for a key
    int keyToSearch = 15;
    if (hashTable.search(keyToSearch)) {
        std::cout << keyToSearch << " found in the hash table." << std::endl;
    } else {
        std::cout << keyToSearch << " not found in the hash table." << std::endl;
    }

    // Remove a key
    int keyToRemove = 25;
    hashTable.remove(keyToRemove);
    std::cout << "Hash Table after removing " << keyToRemove << ":" << std::endl;
    hashTable.display();

    return 0;
}
