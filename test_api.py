path_key = "filename"
change_key = "patch"
headers = {
    "Authorization": f"Bearer ",
    "X-GitHub-Api-Version": "2022-11-28",
    "Accept": "application/vnd.github+json"
}
context = ssl.create_default_context()
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE

req = urllib.request.Request(api_req_url, headers=headers)
try:
    with urllib.request.urlopen(req, context=context) as response:
        change_response = response.read()
except Exception as e:
    print(f"Unable to get URL {api_req_url}")
    raise e

change_response = json.loads(change_response)
changes = change_response

for change in changes:
    if path_key in change and change_key in change:
        path_to_be_excluded = self.__is_excluded_path(change[path_key])
        if path_to_be_excluded is False:
            curr_file = os.path.join(self.temp_dir.name, change[path_key])
            curr_dir = os.path.dirname(curr_file)
            if curr_dir != self.temp_dir.name:
                os.makedirs(name=curr_dir, exist_ok=True)
            curr_file = open(file=curr_file, mode='w+', encoding='UTF-8')
            print(re.sub(remove_diff_regex, r"\2", change[change_key]),
                file=curr_file)