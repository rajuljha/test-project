#include <iostream>
#include <vector>

class DoubleHashingHashTable {
private:
    static const int TABLE_SIZE = 10;
    std::vector<int> table;

    // Secondary hash function for double hashing
    int secondaryHashFunction(int key) {
        // Using a prime number less than TABLE_SIZE for simplicity
        return 7 - (key % 7);
    }

public:
    DoubleHashingHashTable() {
        table.resize(TABLE_SIZE, -1); // Initialize all slots to -1 (indicating empty)
    }

    int primaryHashFunction(int key) {
        return key % TABLE_SIZE;
    }

    void insert(int key) {
        int index = primaryHashFunction(key);

        if (table[index] == -1) {
            // If the slot is empty, insert the key
            table[index] = key;
        } else {
            // Collision: use double hashing for probing
            int step = secondaryHashFunction(key);
            int originalIndex = index;

            do {
                index = (index + step) % TABLE_SIZE;

                if (table[index] == -1) {
                    // Found an empty slot, insert the key
                    table[index] = key;
                    return;
                }
            } while (index != originalIndex);
            
            std::cerr << "Hash table is full. Unable to insert key: " << key << std::endl;
        }
    }

    bool search(int key) {
        int index = primaryHashFunction(key);
        int step = secondaryHashFunction(key);
        int originalIndex = index;

        do {
            if (table[index] == key) {
                return true; // Key found
            }

            index = (index + step) % TABLE_SIZE;
        } while (table[index] != -1 && index != originalIndex);

        return false; // Key not found
    }

    void display() {
        std::cout << "Hash Table (Double Hashing): ";
        for (int i = 0; i < TABLE_SIZE; ++i) {
            if (table[i] != -1) {
                std::cout << table[i] << " ";
            } else {
                std::cout << "- ";
            }
        }
        std::cout << std::endl;
    }
};

int main() {
    DoubleHashingHashTable doubleHashingTable;

    doubleHashingTable.insert(5);
    doubleHashingTable.insert(15);
    doubleHashingTable.insert(25);
    doubleHashingTable.insert(35);

    doubleHashingTable.display();

    int keyToSearch = 15;
    if (doubleHashingTable.search(keyToSearch)) {
        std::cout << keyToSearch << " found in the hash table." << std::endl;
    } else {
        std::cout << keyToSearch << " not found in the hash table." << std::endl;
    }

    return 0;
}
