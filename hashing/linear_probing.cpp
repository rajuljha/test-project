#include <iostream>
#include <vector>

class LinearProbingHashTable {
private:
    static const int TABLE_SIZE = 10;
    std::vector<int> table;

public:
    LinearProbingHashTable() {
        table.resize(TABLE_SIZE, -1); // Initialize all slots to -1 (indicating empty)
    }

    int hashFunction(int key) {
        return key % TABLE_SIZE;
    }

    void insert(int key) {
        int index = hashFunction(key);

        while (table[index] != -1) {
            // Linear probing: move to the next slot
            index = (index + 1) % TABLE_SIZE;
        }

        table[index] = key;
    }

    bool search(int key) {
        int index = hashFunction(key);

        while (table[index] != -1) {
            if (table[index] == key) {
                return true; // Key found
            }

            // Linear probing: move to the next slot
            index = (index + 1) % TABLE_SIZE;
        }

        return false; // Key not found
    }

    void display() {
        std::cout << "Hash Table (Linear Probing): ";
        for (int i = 0; i < TABLE_SIZE; ++i) {
            if (table[i] != -1) {
                std::cout << table[i] << " ";
            } else {
                std::cout << "- ";
            }
        }
        std::cout << std::endl;
    }
};

int main() {
    LinearProbingHashTable linearProbingTable;

    linearProbingTable.insert(5);
    linearProbingTable.insert(15);
    linearProbingTable.insert(25);
    linearProbingTable.insert(35);

    linearProbingTable.display();

    int keyToSearch = 15;
    if (linearProbingTable.search(keyToSearch)) {
        std::cout << keyToSearch << " found in the hash table." << std::endl;
    } else {
        std::cout << keyToSearch << " not found in the hash table." << std::endl;
    }

    return 0;
}
