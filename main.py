from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'hello'

# Copyright 2024 @ John Cena
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
# orthogonally
# patent
# Licensed under
