#include <iostream>
#include <vector>

class MinHeap {
private:
    std::vector<int> heap;

    // Helper function to heapify a subtree rooted with the given index
    void heapify(int index) {
        int smallest = index;           // Initialize smallest as root
        int leftChild = 2 * index + 1;   // Left child
        int rightChild = 2 * index + 2;  // Right child

        // Compare with left child
        if (leftChild < heap.size() && heap[leftChild] < heap[smallest]) {
            smallest = leftChild;
        }

        // Compare with right child
        if (rightChild < heap.size() && heap[rightChild] < heap[smallest]) {
            smallest = rightChild;
        }

        // If the smallest is not the root, swap and recursively heapify the affected subtree
        if (smallest != index) {
            std::swap(heap[index], heap[smallest]);
            heapify(smallest);
        }
    }

public:
    // Constructor to create an empty heap
    MinHeap() {}

    // Function to insert a new element into the heap
    void insert(int value) {
        heap.push_back(value);  // Insert the new element at the end

        int index = heap.size() - 1;  // Index of the inserted element

        // Fix the Min Heap property
        while (index > 0 && heap[(index - 1) / 2] > heap[index]) {
            std::swap(heap[index], heap[(index - 1) / 2]);
            index = (index - 1) / 2;
        }
    }

    // Function to extract the minimum element (root) from the heap
    int extractMin() {
        if (heap.empty()) {
            std::cerr << "Heap is empty. Cannot extract min." << std::endl;
            return -1; // Assuming -1 as an indicator for an error
        }

        int minElement = heap[0];  // Minimum element is at the root

        // Replace the root with the last element
        heap[0] = heap.back();
        heap.pop_back();

        // Fix the Min Heap property
        heapify(0);

        return minElement;
    }

    // Function to display the elements of the heap
    void display() {
        std::cout << "Min Heap: ";
        for (int value : heap) {
            std::cout << value << " ";
        }
        std::cout << std::endl;
    }
};

int main() {
    MinHeap minHeap;

    minHeap.insert(5);
    minHeap.insert(10);
    minHeap.insert(7);
    minHeap.insert(3);
    minHeap.insert(1);

    minHeap.display();

    std::cout << "Extracted Min: " << minHeap.extractMin() << std::endl;
    
    minHeap.display();

    return 0;
}
