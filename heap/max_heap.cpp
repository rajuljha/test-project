#include <iostream>
#include <vector>

class MaxHeap {
private:
    std::vector<int> heap;

    // Helper function to heapify a subtree rooted with the given index
    void heapify(int index) {
        int largest = index;        // Initialize largest as root
        int leftChild = 2 * index + 1;   // Left child
        int rightChild = 2 * index + 2;  // Right child

        // Compare with left child
        if (leftChild < heap.size() && heap[leftChild] > heap[largest]) {
            largest = leftChild;
        }

        // Compare with right child
        if (rightChild < heap.size() && heap[rightChild] > heap[largest]) {
            largest = rightChild;
        }

        // If the largest is not the root, swap and recursively heapify the affected subtree
        if (largest != index) {
            std::swap(heap[index], heap[largest]);
            heapify(largest);
        }
    }

public:
    // Constructor to create an empty heap
    MaxHeap() {}

    // Function to insert a new element into the heap
    void insert(int value) {
        heap.push_back(value);  // Insert the new element at the end

        int index = heap.size() - 1;  // Index of the inserted element

        // Fix the Max Heap property
        while (index > 0 && heap[(index - 1) / 2] < heap[index]) {
            std::swap(heap[index], heap[(index - 1) / 2]);
            index = (index - 1) / 2;
        }
    }

    // Function to extract the maximum element (root) from the heap
    int extractMax() {
        if (heap.empty()) {
            std::cerr << "Heap is empty. Cannot extract max." << std::endl;
            return -1; // Assuming -1 as an indicator for an error
        }

        int maxElement = heap[0];  // Maximum element is at the root

        // Replace the root with the last element
        heap[0] = heap.back();
        heap.pop_back();

        // Fix the Max Heap property
        heapify(0);

        return maxElement;
    }

    // Function to display the elements of the heap
    void display() {
        std::cout << "Max Heap: ";
        for (int value : heap) {
            std::cout << value << " ";
        }
        std::cout << std::endl;
    }
};

int main() {
    MaxHeap maxHeap;

    maxHeap.insert(5);
    maxHeap.insert(10);
    maxHeap.insert(7);
    maxHeap.insert(3);
    maxHeap.insert(1);

    maxHeap.display();

    std::cout << "Extracted Max: " << maxHeap.extractMax() << std::endl;
    
    maxHeap.display();

    return 0;
}
